//
//  ParcelService.swift
//  swish-client
//
//  Created by Owen Ancelot on 27/02/2023.
//

import Foundation

protocol ParcelService {
    
    func getParcelByUuid(uuid: UUID, _ completion: @escaping (Parcel?, Error?) -> Void);
    
}
