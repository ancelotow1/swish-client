//
//  HttpMethod.swift
//  swish-client
//
//  Created by Owen Ancelot on 27/02/2023.
//

import Foundation

enum HttpMethod: String {
    
    case GET
    case POST
    
}
