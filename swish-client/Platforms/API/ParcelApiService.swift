//
//  ParcelApiService.swift
//  swish-client
//
//  Created by Owen Ancelot on 27/02/2023.
//

import Foundation

class ParcelApiService: ParcelService {
    
    func getParcelByUuid(uuid: UUID, _ completion: @escaping (Parcel?, Error?) -> Void) {
        let apiCaller = ApiCaller(endpoint: "parcel/\(uuid)", method: HttpMethod.GET).withJwtToken();
        apiCaller.execute() { data, err in
            guard err == nil else {
                completion(nil, err)
                return
            }
            guard let d = data else {
                completion(nil, NSError(domain: "com.esgi.swish.client", code: 2, userInfo: [
                    NSLocalizedFailureReasonErrorKey: "Aucune données trouvées"
                ]))
                return
            }
            do {
                let json = try JSONSerialization.jsonObject(with: d, options: .allowFragments)
                guard let dict = json as? [String: Any] else {
                    completion(nil, NSError(domain: "com.esgi.swish.client", code: 2, userInfo: [
                        NSLocalizedFailureReasonErrorKey: "Erreur sur le format JSON"
                    ]))
                    return
                }
                guard let parcel = Parcel.fromDictionary(dict: dict) else {
                    completion(nil, NSError(domain: "com.esgi.swish.client", code: 2, userInfo: [
                        NSLocalizedFailureReasonErrorKey: "Erreur lors du mappage de l'objet"
                    ]))
                    return
                }
                completion(parcel, nil)
            } catch {
                completion(nil, err)
                return
            }
        }
    }
    
}
