//
//  ApiCaller.swift
//  swish-client
//
//  Created by Owen Ancelot on 27/02/2023.
//

import Foundation

class ApiCaller {
    
    private let apiServer = "https://swish.ancelotow.com/api/v1/"
    private var request: URLRequest
    
    
    init(endpoint: String, method: HttpMethod) {
        let uri = URLComponents(string: apiServer + endpoint)
        request = URLRequest(url: uri!.url!)
        request.httpMethod = method.rawValue
        request.addValue("application/json", forHTTPHeaderField: "Accept")
    }
    
    func withJsonBody(body: [String: Any]) -> ApiCaller {
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpBody = try? JSONSerialization.data(withJSONObject: body)
        return self
    }
    
    func withJwtToken() -> ApiCaller {
        let token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJsb2dpbiI6bnVsbCwidXVpZCI6bnVsbCwiaXNBZG1pbiI6dHJ1ZSwiaWF0IjoxNjc3NTE4ODU5LCJleHAiOjE2Nzc3NzgwNTl9.DStMvsK628ZLlMu6_naWJCgE75tRFbGPSpU94Ni4wcw"
        request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        return self
    }
    
    func execute(_ completion: @escaping (Data?, Error?) -> Void) {
        let task = URLSession.shared.dataTask(with: request) { data, response, err in
            guard err == nil else {
                completion(nil, err)
                return
            }
            guard let httpResponse = response as? HTTPURLResponse else {
                completion(nil, NSError(domain: "com.esgi.swish.client", code: 2, userInfo: [
                    NSLocalizedFailureReasonErrorKey: "Aucune données trouvées"
                ]))
                return
            }
            var statusCode = httpResponse.statusCode
            if statusCode == 500 {
                completion(nil, NSError(domain: "com.esgi.swish.client", code: 2, userInfo: [
                    NSLocalizedFailureReasonErrorKey: "Erreur 500 - Internal Server Error"
                ]))
                return
            }
            if statusCode == 404 {
                completion(nil, NSError(domain: "com.esgi.swish.client", code: 2, userInfo: [
                    NSLocalizedFailureReasonErrorKey: "Erreur 404 - Not Found"
                ]))
                return
            }
            if statusCode == 401 {
                completion(nil, NSError(domain: "com.esgi.swish.client", code: 2, userInfo: [
                    NSLocalizedFailureReasonErrorKey: "Erreur 401 - Unauthorized"
                ]))
                return
            }
            if statusCode == 403 {
                completion(nil, NSError(domain: "com.esgi.swish.client", code: 2, userInfo: [
                    NSLocalizedFailureReasonErrorKey: "Erreur 403 - Forbidden"
                ]))
                return
            }
            if statusCode >= 400 {
                completion(nil, NSError(domain: "com.esgi.swish.client", code: 2, userInfo: [
                    NSLocalizedFailureReasonErrorKey: String(bytes: data!, encoding: String.Encoding.utf8)
                ]))
                return
            }
            if statusCode == 204 {
                completion(nil, nil)
                return
            }
            completion(data, nil)
        }
        task.resume()
    }
    
}
