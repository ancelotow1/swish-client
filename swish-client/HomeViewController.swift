//
//  HomeViewController.swift
//  swish-client
//
//  Created by Owen Ancelot on 26/02/2023.
//

import UIKit
import MapKit

class HomeViewController: UIViewController {

    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var iconParcelState: UIImageView!
    @IBOutlet weak var parcelState: UILabel!
    @IBOutlet weak var labelFullname: UILabel!
    @IBOutlet weak var searchField: UITextField!
    var parcel: Parcel?
    let parcelService: ParcelService = ParcelApiService()
    var loadingAlert: UIAlertController? = nil
    var coordinate: CLLocationCoordinate2D?
    var locationManager: CLLocationManager?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
        self.iconParcelState.isHidden = true
        self.parcelState.isHidden = true
        self.labelFullname.isHidden = true
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true) // Close keyboard.
    }
    
    func showErrorAlertWithMessage(_ message: String) {
        let alert = UIAlertController(title: "Invalid", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Fermer", style: .cancel))
        self.present(alert, animated: true)
    }
    
    func showLoadingAlert() {
        let recoveryMsg = "Récupération..."
        self.loadingAlert = UIAlertController(title: nil, message: recoveryMsg, preferredStyle: .alert)
        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.medium
        loadingIndicator.startAnimating();
        self.loadingAlert!.view.addSubview(loadingIndicator)
        present(self.loadingAlert!, animated: true, completion: nil)
    }
    
    func dismissLoadingAlert(_ completion: @escaping () -> Void) {
        guard let alert = self.loadingAlert else {
            return
        }
        alert.dismiss(animated: true, completion: completion)
        self.loadingAlert = nil
    }
    
    func loadFields() {
        guard let parcel = self.parcel else {
            return
        }
        self.iconParcelState.isHidden = false
        self.parcelState.isHidden = false
        self.labelFullname.isHidden = false
        self.labelFullname.text = parcel.getFullname()
        if parcel.isDelivered {
            self.iconParcelState.image = UIImage(systemName: "cube.box.fill")
            self.iconParcelState.tintColor = .systemGreen
            let formatter = DateFormatter()
            formatter.dateFormat = "dd/MM/yyyy HH:mm"
            var dateStr = formatter.string(from: parcel.dateDelivered!)
            self.parcelState.text = "Livré : \(dateStr)"
        } else {
            self.iconParcelState.image = UIImage(systemName: "box.truck.badge.clock.fill")
            self.iconParcelState.tintColor = .systemGreen
        }
    }
    
    func askLocationPermission() {
        let manager = CLLocationManager()
        manager.delegate = self
        if manager.authorizationStatus == .notDetermined {
            manager.requestWhenInUseAuthorization()
            
        } else {
            print(manager.authorizationStatus)
        }
        self.locationManager = manager
    }

    @IBAction func handleSearch(_ sender: Any) {
        
        guard let uuidStr = self.searchField.text,
              !uuidStr.isEmpty else {
            self.showErrorAlertWithMessage("Vous devez saisir un Track ID")
            return
        }
        guard let uuid = UUID(uuidString: uuidStr) else {
            self.showErrorAlertWithMessage("Vous devez saisir un Track ID valide")
            return
        }
        self.showLoadingAlert()
        self.parcelService.getParcelByUuid(uuid: uuid) { data, err in
            guard err == nil else {
                DispatchQueue.main.async {
                    self.dismissLoadingAlert() {
                        let message = (err as? NSError)?.localizedFailureReason ?? "Erreur inconnue"
                        self.showErrorAlertWithMessage(message)
                    }
                }
                return
            }
            guard let data = data else {
                DispatchQueue.main.async {
                    self.dismissLoadingAlert() {
                        self.showErrorAlertWithMessage("Aucun colis trouvé avec ce Track ID")
                    }
                }
                return
            }
            self.parcel = data
            DispatchQueue.main.async {
                self.dismissLoadingAlert() {
                    self.loadFields()
                }
            }
        }
    }
}

extension HomeViewController: CLLocationManagerDelegate {
    func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
        self.hasLocationManagerStatus(manager)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let lastLocation = locations.last else {
            return
        }
        self.coordinate = lastLocation.coordinate
        manager.stopUpdatingLocation()
    }
    
    func hasLocationManagerStatus(_ manager: CLLocationManager) {
        if manager.authorizationStatus == .restricted || manager.authorizationStatus == .denied {
            self.displayDeniedMessage()
        } else if manager.authorizationStatus == .authorizedWhenInUse || manager.authorizationStatus == .authorizedAlways {
            self.handleGPSCoordinate(manager)
        }
    }
    
    func displayDeniedMessage() {
        let title = "Localisation refusée"
        let message = "Vous devez activer la localisation dans les préférences du téléphone"
        let cancelLabel = "Annuler"
        let openPrefs = "Ouvrir les préférences"
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: cancelLabel, style: .cancel))
        alert.addAction(UIAlertAction(title: openPrefs, style: .default, handler: { _ in
            UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
        }))
        self.present(alert, animated: true)
    }
    
    func handleGPSCoordinate(_ manager: CLLocationManager) {
        manager.startUpdatingLocation()
    }

}

